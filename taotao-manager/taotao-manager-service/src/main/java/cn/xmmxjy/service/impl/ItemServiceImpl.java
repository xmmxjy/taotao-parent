package cn.xmmxjy.service.impl;

import cn.xmmxjy.mapper.TbItemMapper;
import cn.xmmxjy.pojo.TbItem;
import cn.xmmxjy.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by xmm on 2017/5/29.
 * 商品管理service
 */
@Service
public class ItemServiceImpl implements ItemService{

    @Autowired
    private TbItemMapper itemMapper;

    @Override
    public TbItem getItemById(Long id) {
        return itemMapper.selectByPrimaryKey(id);
    }
}
