package cn.xmmxjy.service;

import cn.xmmxjy.pojo.TbItem;

/**
 * Created by xmm on 2017/5/29.
 */
public interface ItemService {

    TbItem getItemById(Long id);
}
